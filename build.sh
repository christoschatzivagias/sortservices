#!/usr/bin/sh

sudo docker stop sort-db sort-rest sort-server sort-frontend
sudo docker rm sort-db sort-rest sort-server sort-frontend
rm -rf sortrest/target/
rm -rf sortfrontend/dist/
cd sortfrontend && npm install && ng build --prod
cd ../
cd sortrest && mvn package
cd ../
sudo docker-compose -f docker-compose.yml up --build

