import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HistoryComponent } from "./history/history.component"
import { AppComponent } from './app.component';
import { SortComponent } from './sort/sort.component';

const routes: Routes = [
  { path: '', component: SortComponent },
  { path: 'history', component: HistoryComponent },
  { path: 'sort', component: SortComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
