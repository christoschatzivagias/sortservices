import { Component, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-sort',
  templateUrl: './sort.component.html',
  styleUrls: ['./sort.component.css']
})



export class SortComponent implements OnInit {

  numberArray:string = '';
  sortedNumberArray:string = '';
  url = environment.url + '/api/numberarrays'
  sortWaitIndicatorVisibility:boolean = false;
  numberArrayValidity:boolean = true;

  constructor(private http: HttpClient) { }

  isNumber(str) {
    return !isNaN(parseFloat(str)) && isFinite(str);
  }

  ngOnInit() {
  }



  sortRequest() {
    if(this.numberArray.split(",").filter(element => !this.isNumber(element)).length == 0) {
      this.sortWaitIndicatorVisibility = true;
  
      this.http.post(this.url, {"original":this.numberArray}).toPromise().then(data => {
        this.sortedNumberArray = data['sorted'];
      });
  
      setTimeout( () => { this.sortWaitIndicatorVisibility = false; }, 500 );
    } else {
      this.sortedNumberArray = "Invalid Input Array"
    }
  } 

}
