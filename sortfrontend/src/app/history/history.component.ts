import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  sortHistoryList:object = null;
  fetchWaitIndicatorVisibility:boolean = false;

  url = environment.url + '/api/numberarrays'

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  fetchHistoryRequest() {
    this.fetchWaitIndicatorVisibility = true
    this.http.get(this.url).toPromise().then(data => {
      this.sortHistoryList = data['content'];

    })
    setTimeout( () => { this.fetchWaitIndicatorVisibility = false; }, 2000 );

  }

}
