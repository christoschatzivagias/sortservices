#!/usr/bin/sh

kubectl apply -f sort-server-service.yaml 
kubectl apply -f sort-rest-service.yaml
kubectl apply -f sort-frontend-service.yaml
kubectl apply -f sort-db-service.yaml
kubectl apply -f sort-server-deployment.yaml 
kubectl apply -f sort-rest-deployment.yaml
kubectl apply -f sort-frontend-deployment.yaml
kubectl apply -f sort-db-deployment.yaml

