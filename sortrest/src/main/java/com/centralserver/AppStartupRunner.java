package com.centralserver;

import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class AppStartupRunner implements ApplicationRunner {

    static public WebSocketClient client = null;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        client = new WebSocketClient();
        try
        {
            client.start();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
        }

    }
}
