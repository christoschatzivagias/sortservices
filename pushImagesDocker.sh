#!/usr/bin/sh

rm -rf sortrest/target/
rm -rf sortfrontend/dist/
cd sortfrontend && npm install && ng build --prod
cd ../
cd sortrest && mvn package
cd ../

sudo docker login
sudo docker-compose -f docker-compose.yml build
sudo docker tag sort-server chatzich/sort-server
sudo docker tag sort-rest chatzich/sort-rest
sudo docker tag sort-frontend chatzich/sort-frontend
sudo docker push chatzich/sort-server
sudo docker push chatzich/sort-rest
sudo docker push chatzich/sort-frontend

