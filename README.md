# Sort Services

  ![Screenshot](sortservicesdiagram.png)

  ## sort-rest
  The server which receives HTTP sort requests from frontend/nginx
 
  Default ports:
    Developement:8080
    Production:8080
  
  Deployment:
 
   Use of **openjdk version 1.8.0_222** and **maven**. Just change directory to sortrest and run `mvn spring-boot:run`
  
  `cd sortrest`

  `mvn spring-boot:run`


  ## sort-server
  The server which is the CUDA/C++ sort process. There is an implementation of a server that receives **WEB
  SOCKET** messages from ***sort-rest*** server with the unsorted array and executes the sorting with CUDA algorithm or
  CPU sorting. The CPU sorting is enabled when there is no CUDA developemnt kit or Driver in the deployment machine.
  CMake build system used to build this application, **CMakeLists.txt** contains a check if CUDA is available in the machine
  and also a flag USE_CUDA is provided for manually setup the use of CUDA 
  The default port the server is listening is 1234 but the is an argument option -p PORT
  
  Default ports:
    Development:1234
    Production:1234
  
  Deployment:
  Change to sortserver directory and make a directory with name build 
  execute `cmake ../ -DCMAKE_RELEASE_TYPE=Release` and then run `make`
  
  `cd sortserver`

  `mkdir build`

  `cd build`
  
  `cmake ../ -DCMAKE_RELEASE_TYPE=Release`
  
  `make`

  `./build/sortserver`




  ## sort-db
  The postgress database server.
  Simple use of postgres service
  
  Default ports:
    Developement:5432
    Production:5432

  ## sort-frontend
  Written in angular with two components:
  1.The sort component that handles the sort request to the ***rest-server***
  2.The history component that handles the fetch-all-sorts request to the ***rest-server***
  
  Default ports:
    Development:4200
    Production:80
    
  Deployment:
   
  `cd sortfrontend`

  `ng serve`
  

# Tests
  ## sort-server:
  I have made 2 tests for gpu and cpu mode
  The first test creates an index with a reverse sorted matrix i.e [10,9,8,7,6,5,4,3,2,1] and validates
  the result with the sorted from 1 to 10.
  The Second test creates a fixed size array with random numbers and validates it by checking if the Nth index 
  element is greater or equal to (N-1)th element
  
  `mkdir build`
  
  `cd build`
  
  `cmake ../ -Dtest=ON`
  
  `make`
  
  `make test`
  
  ## sort-rest:
   You have to install pytest:
   
   `sudo apt install python-pytest`
   
   and pip requirements for python
   
   `sudo pip install -r requirements.txt`
   
   on each test folder tests/integration.txt and tests/acceptance/requirements.txt
   
   you must also run **downloaddriver.sh** on the tests/acceptance folder to setup
   the webdriver used by selenium to open firefox in robot mode
   
   to run the tests just change to which directory integration or acceptance and 
   execute
   
   `pytest test1.py`
   
   
   
   ### Integration Tests:
   
   1.**post_sort_test**:
     Makes a post request with a randomly generated array of  1000 items and checks if the response returns
     the sorted array properly
      
   2.**get_sort_history_request**:
     Makes a simple post request with specific numbers, after that makes a second get request and checks if
     the last item is the newly inserted one
      
   ### Acceptance Tests:
   
   1.**test_app_open**:
     Opens the app and checks if the app element exists
      
   2.**test_sort_post_response_valid**:
     Fills unsorted textarea with a simple list of numbers clicks the sort button 
     waits for the answer on sorted text area and validates the results
    
   3.**test_app_history_exists**:
     Opens the app and checks if the history link element exists

   4.**test_app_history_valid_response**:
     Initially makes a POST request with a specific unsorted list
     then it clicks the history link element to display the sort history mode
     after that clicks the Fetch History Button to display the history elements
     and validates if the last element is the unsorted list posted on the initial step

## Docker build
Just run **build.sh** to run the sort services

## Kubernetes build
The four docker images are pushed on my docker hub repository **chatzich/sort-db**, **chatzich/sort-server**, 
**chatzich/sort-rest**, **chatzich/sort-frontend**. Change to directory kubernetes and run **deploy.sh** to create
the services and deployments
The **helm charts** are in folder **charts**

# Desktop Application
  I have also created a Desktop Application Client which has
  the same functionality that frontend has
  1.Fetches history of the sort requests making a request to the ***rest-server***
  2.Posts sort requests with unsorted arrays, and wait for response from ***rest-server***


