#!/usr/bin/sh

docker run -d -p 5000:5000 --restart=always --name sort_services_registry registry:2
docker-compose build
docker tag sort-server localhost:5000/sort-server
docker push localhost:5000/sort-server
docker tag sort-rest localhost:5000/sort-rest
docker push localhost:5000/sort-test
docker tag sort-frontend localhost:5000/sort-frontend
docker push localhost:5000/sort-frontend

