import math
import requests

import argparse
import random



args = {}
args['host'] = "localhost"
args['port'] = "8080"


def test_post_sort():
   strlist = []
   for x in range(1000):
   	strlist.append(str(random.randint(1,1001)))
   print(strlist)
   data = '{"original":"' + (",").join(strlist) + '"}'
   req = requests.post("http://" + args['host'] + ":" + args["port"] + "/api/numberarrays", data=data, headers = {'Content-type': 'application/json'}).json()
   sortedlist = sorted(list(map(lambda x:int(x), strlist)))
   print(req['sorted'])
   assert req['sorted'] == (",").join(list(map(lambda x:str(x),sortedlist)))


def test_get_sort_history_request():
   originalstrlist = "999,666,333"
   sortedstrlist = "333,666,999"
   data = '{"original":"'+ originalstrlist +'"}'
   requests.post("http://" + args['host'] + ":" + args["port"] + "/api/numberarrays", data=data, headers = {'Content-type': 'application/json'})
   req = requests.get("http://" + args['host'] + ":" + args["port"] + "/api/numberarrays", headers = {'Content-type': 'application/json'}).json()
   assert(req['content'][-1]['original'] == originalstrlist)
   assert(req['content'][-1]['sorted'] == sortedstrlist)
