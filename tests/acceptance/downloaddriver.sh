#!/usr/bin/sh

wget https://github.com/mozilla/geckodriver/releases/download/v0.25.0/geckodriver-v0.25.0-linux64.tar.gz
tar -xvf geckodriver-v0.25.0-linux64.tar.gz
sudo mv geckodriver /usr/bin
