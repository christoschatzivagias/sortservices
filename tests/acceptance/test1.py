import math
import requests
from   selenium import webdriver
from   selenium.common.exceptions import TimeoutException
import argparse
import random


parser = argparse.ArgumentParser(description='Test a server')
parser.add_argument('--host', help='host')
parser.add_argument('--port', help='port')

try:
    args = parser.parse_args()
except:
    pass

def test_app_open():
    browser = webdriver.Firefox()
    browser.get("http://localhost:81")
    browser.implicitly_wait(5)
    applabel = browser.find_element_by_class_name("logo")
    assert(applabel.text == "SortApp")
    browser.close()

def test_sort_post_response_valid():
    originalstrlist = "999,666,333"
    sortedstrlist = "333,666,999"
    browser = webdriver.Firefox()
    browser.get("http://localhost:81")
    browser.implicitly_wait(5)
    textareas = browser.find_elements_by_tag_name("textarea")
    unsort_area = textareas[0]
    sort_area = textareas[1]
    unsort_area.send_keys(originalstrlist)
    sort_button = browser.find_element_by_tag_name("button")
    sort_button.click();
    browser.implicitly_wait(10)
    assert(sort_area.get_attribute('value') == sortedstrlist)
    browser.close()

def test_app_history_exists():
    browser = webdriver.Firefox()
    browser.get("http://localhost:81")
    browser.implicitly_wait(5)
    historyLink = browser.find_element_by_xpath('//a[contains(@href,"/history")]')
    assert(historyLink != None)
    browser.close()

   
  
def test_app_history_valid_response():
    originalstrlist = "999,666,333"
    sortedstrlist = "333,666,999"

    data = '{"original":"' + originalstrlist + '"}'
    requests.post("http://localhost:8080/api/numberarrays", data=data, headers = {'Content-type': 'application/json'})
    browser = webdriver.Firefox()
    browser.get("http://localhost:81/history")
    browser.implicitly_wait(5)
    historyLink = browser.find_element_by_xpath('//a[contains(@href,"/history")]')
    historyLink.click()
    browser.implicitly_wait(5)
    fetchHistoryButton = browser.find_element_by_tag_name("button")
    fetchHistoryButton.click()
    browser.implicitly_wait(10)
    historyItems = browser.find_elements_by_class_name('WrapText')
    assert(historyItems[-1].text == sortedstrlist)
    browser.close()



