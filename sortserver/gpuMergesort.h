#include <stdio.h>
#include <time.h>

extern int mergesort_gpu(double* data, long size, int xThreadPerBlock = 32, int xBlocksPerGrid = 8);
