#!/usr/bin/sh

rm /sortserver/CMakeCache.txt
cd /sortserver
mkdir build  
cd build
cmake ../ -DCMAKE_BUILD_TYPE=Release -Dtest=ON
make -j
cd ../
mkdir build_test 
cd build_test
cmake ../ -DCMAKE_BUILD_TYPE=Debug -Dtest=ON
make -j
make -j test

/sortserver/build/sortserver
