#include <iostream>
#include <gtest/gtest.h>
#include "gpuMergesort.h"
#define ARRAY_SIZE 100
TEST(AddTest, GPU_Test1){

	double rightsorted[ARRAY_SIZE];
	for(int i = 0; i < ARRAY_SIZE; i++) 
		rightsorted[i] = i;
	
	double unsorted[ARRAY_SIZE];
	for(int i = ARRAY_SIZE - 1; i >= 0; i--) 
		unsorted[i] = ARRAY_SIZE - 1 - i;

	double testsorted[ARRAY_SIZE];
	mergesort_gpu(unsorted, ARRAY_SIZE);
	
	
	for(int index = 0; index < ARRAY_SIZE; index++) {
		ASSERT_EQ(rightsorted[index], unsorted[index]);
	}
}

TEST(AddTest, GPU_Test2)
{
	 srand(time(0));

	 double unsorted[ARRAY_SIZE];
	 for(int i = 0; i<ARRAY_SIZE; i++)
		 unsorted[i] = rand();

	mergesort_gpu(unsorted, ARRAY_SIZE);
	
	for(int index = 1; index < ARRAY_SIZE; index++) {
		ASSERT_EQ(unsorted[index-1] <= unsorted[index], true);
	}

}

int main(int argc, char *argv[]) {
	::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
