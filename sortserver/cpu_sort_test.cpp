#include <iostream>
#include <gtest/gtest.h>
#include "cpuMergesort.h"
#define ARRAY_SIZE 100000

TEST(AddTest, CPU_Test1){
	double rightsorted[ARRAY_SIZE];
	for(int i = 0; i < ARRAY_SIZE; i++) 
		rightsorted[i] = i;
	
	double unsorted[ARRAY_SIZE];
	for(int i = ARRAY_SIZE - 1; i >= 0; i--) 
		unsorted[i] = ARRAY_SIZE - 1 - i;

	double sorted[ARRAY_SIZE];
	double testsorted[ARRAY_SIZE];
	mergesort(unsorted, sorted, ARRAY_SIZE);
	
	
	for(int index = 0; index < ARRAY_SIZE; index++) {
		ASSERT_EQ(rightsorted[index], sorted[index]);
	}

}



TEST(AddTest, CPU_Test2)
{
	 srand(time(0));

	 double unsorted[ARRAY_SIZE];
	 for(int i = 0; i<ARRAY_SIZE; i++)
		 unsorted[i] = rand();

	double sorted[ARRAY_SIZE];
	mergesort(unsorted, sorted, ARRAY_SIZE);
	
	for(int index = 1; index < ARRAY_SIZE; index++) {
		ASSERT_EQ(sorted[index-1] <= sorted[index], true);
	}
}





int main(int argc, char *argv[]) {
	::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
