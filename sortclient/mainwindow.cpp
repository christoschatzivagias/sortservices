#include "mainwindow.h"
#include <QPlainTextEdit>
#include <QHBoxLayout>
#include <QToolButton>
#include <QLineEdit>
#include <QProgressBar>
#include <QLabel>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QStatusBar>
#include <QTabWidget>
#include <QTableWidget>
#include <QPushButton>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QMenu>
#include <QClipboard>
#include <QApplication>
#include <QMessageBox>
#include <QPlainTextEdit>
#include <QSplitter>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    QFrame *mp_centralFrame = new QFrame(this);
    mp_centralFrame->setLayout(new QVBoxLayout);	
    mp_urlFrame = new QFrame(mp_centralFrame);
    mp_urlFrame->setLayout(new QHBoxLayout);
    mp_serverLabel = new QLabel("Sort-Server URL:",mp_urlFrame);
    mp_urlFrame->layout()->addWidget(mp_serverLabel);
    mp_urlEdit = new QLineEdit(mp_urlFrame);
    mp_urlEdit->setText("http://localhost:8080/api/numberarrays");

    mp_urlFrame->layout()->addWidget(mp_urlEdit);

    mp_centralFrame->layout()->addWidget(mp_urlFrame);
    QTabWidget *mp_tabWidget = new QTabWidget(this);
    mp_centralFrame->layout()->addWidget(mp_tabWidget);
    mp_sortFrame = new QFrame(mp_tabWidget);
    mp_sortFrame->setLayout(new QVBoxLayout);
    mp_arrayFrame = new QFrame(mp_sortFrame);
    mp_arrayFrame->setLayout(new QHBoxLayout);
    mp_numberArray = new QPlainTextEdit(mp_arrayFrame);
    mp_arrayFrame->layout()->addWidget(mp_numberArray);
    mp_sortRequestButton = new QToolButton(mp_arrayFrame);
    mp_sortRequestButton->setText("Sort");
    mp_arrayFrame->layout()->addWidget(mp_sortRequestButton);
    connect(mp_sortRequestButton, &QToolButton::clicked, [&]{
        mp_progressBar->setMaximum(0);
        mp_progressBar->setMinimum(0);
        mp_statusBar->showMessage("Awaiting Sort Request...");

        QNetworkRequest req(QUrl(mp_urlEdit->text()));
        req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
	QString jsonStr = "{\"original\":\"" + mp_numberArray->toPlainText() + "\"}";
	qDebug() << jsonStr;
        QNetworkReply *reply = this->m_networkManager.post(req,jsonStr.toUtf8());
        connect(reply, &QNetworkReply::finished, [=]() {
            mp_progressBar->setMaximum(100);

            if(reply->error() == QNetworkReply::NoError)
            {
                QByteArray response = reply->readAll();
                QJsonDocument jsonResponse = QJsonDocument::fromJson(response);
                QJsonObject jsonObject = jsonResponse.object();

                mp_sortedNumberArray->setPlainText(jsonObject["sorted"].toString());
                // do something with the data...
            }
            else // handle error
            {
		QMessageBox::critical(nullptr, "Request Error", mp_urlEdit->text() + ":" + reply->errorString());
              qDebug() << reply->errorString();
            }
            mp_statusBar->clearMessage();

        });


    });
    mp_sortedNumberArray = new QPlainTextEdit(mp_arrayFrame);
    mp_arrayFrame->layout()->addWidget(mp_sortedNumberArray);

    mp_sortFrame->layout()->addWidget(mp_arrayFrame);
    mp_statusBar = new QStatusBar(this);
    mp_progressBar = new QProgressBar(this);
    mp_progressBar->setTextVisible(true);

    mp_statusBar->addPermanentWidget(mp_progressBar);
    mp_tabWidget->addTab(mp_sortFrame, "Sort Request");

    QFrame *mp_sortHistoryFrame = new QFrame(this);
    QSplitter *mp_viewsFrame = new QSplitter(Qt::Horizontal,this);
    mp_sortHistoryFrame->setLayout(new QVBoxLayout);
    mp_sortHistoryView = new QTableWidget(mp_sortHistoryFrame);
    mp_sortHistoryView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(mp_sortHistoryView, &QTableWidget::customContextMenuRequested, [&](){
        QMenu *menu = new QMenu;
        auto a = menu->addAction("Copy");
        QObject::connect(a, &QAction::triggered, [&](){
	    QTableWidgetItem *clickItem = mp_sortHistoryView->currentItem(); 
	    if(clickItem)
	    	qApp->clipboard()->setText(clickItem->text());
            a->deleteLater();
            menu->deleteLater();
        });

        menu->exec(QCursor::pos());
        menu->clear();
    });
    QStringList columns;
    columns <<"ID" << "Original" << "Sorted" << "Created At" << "Updated At";
    mp_sortHistoryView->setColumnCount(columns.count());
    mp_sortHistoryView->setHorizontalHeaderLabels(columns);
    mp_currentText = new QPlainTextEdit(this);

    connect(mp_sortHistoryView, &QTableWidget::currentItemChanged, [&](QTableWidgetItem *current, QTableWidgetItem *previous) {
	if(current->column() == 1 || current->column() == 2)
		mp_currentText->setPlainText(current->text());
    });



    mp_viewsFrame->addWidget(mp_sortHistoryView);
    mp_viewsFrame->addWidget(mp_currentText);
    mp_sortHistoryFrame->layout()->addWidget(mp_viewsFrame);
    QPushButton *mp_fetchSortHistoryButton = new QPushButton("Fetch Sort History", mp_sortHistoryFrame);
    connect(mp_fetchSortHistoryButton, &QPushButton::clicked, [&](){
        QNetworkRequest req(QUrl(mp_urlEdit->text()));
	QNetworkReply *reply2 = this->m_networkManager.get(req);
        connect(reply2, &QNetworkReply::finished, [=]() {
            mp_progressBar->setMaximum(100);

	    mp_sortHistoryView->clearContents();
            if(reply2->error() == QNetworkReply::NoError)
            {
                QByteArray response = reply2->readAll();
                QJsonDocument jsonResponse = QJsonDocument::fromJson(response);
                QJsonObject jsonObj = jsonResponse.object();
                QJsonArray jsonArr = jsonObj["content"].toArray();
		mp_sortHistoryView->setRowCount(jsonArr.count());
		QTableWidgetItem *titem = nullptr;
		for (int index = 0; index < jsonArr.count(); index++) {
			QJsonObject jobj =  jsonArr[index].toObject();
			titem = new QTableWidgetItem(QString().setNum(jobj["id"].toInt()));
			titem->setFlags(titem->flags() ^ Qt::ItemIsEditable);
			mp_sortHistoryView->setItem(index, 0, titem);
			titem = new QTableWidgetItem(jobj["original"].toString());
			titem->setFlags(titem->flags() ^ Qt::ItemIsEditable);
			mp_sortHistoryView->setItem(index, 1, titem);
			titem = new QTableWidgetItem(jobj["sorted"].toString());
			titem->setFlags(titem->flags() ^ Qt::ItemIsEditable);
			mp_sortHistoryView->setItem(index, 2, titem);
			titem = new QTableWidgetItem(jobj["createdAt"].toString());
			titem->setFlags(titem->flags() ^ Qt::ItemIsEditable);
			mp_sortHistoryView->setItem(index, 3, titem);
			titem = new QTableWidgetItem(jobj["updatedAt"].toString());
			titem->setFlags(titem->flags() ^ Qt::ItemIsEditable);
			mp_sortHistoryView->setItem(index, 4, titem);
		}
                // do something with the data...
            }
            else // handle error
            {
		QMessageBox::critical(nullptr, "Request Error", mp_urlEdit->text() + ":" + reply2->errorString());
              qDebug() << reply2->errorString();
            }
            mp_statusBar->clearMessage();

        });

    });
    mp_sortHistoryFrame->layout()->addWidget(mp_fetchSortHistoryButton);

    mp_tabWidget->addTab(mp_sortHistoryFrame, "Sort History");
    this->setCentralWidget(mp_centralFrame);
    this->setStatusBar(mp_statusBar);
}

MainWindow::~MainWindow()
{
}
