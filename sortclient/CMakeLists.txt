cmake_minimum_required (VERSION 3.3)
project(sortclient)

set(CMAKE_CXX_STANDARD 14)
enable_language(CXX)
find_package(Qt5 COMPONENTS Core Widgets Network REQUIRED)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)



set(SORT_HEADER_FILES
	mainwindow.h
 )

set(SORT_SRC_FILES
	mainwindow.cpp
 )


add_executable(sortclient main.cpp ${SORT_HEADER_FILES} ${SORT_SRC_FILES})

qt5_use_modules(sortclient Core Widgets Network)
