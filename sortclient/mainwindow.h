#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtNetwork/QNetworkAccessManager>
class QFrame;
class QLabel;
class QLineEdit;
class QPlainTextEdit;
class QToolButton;
class QStatusBar;
class QProgressBar;
class QTableWidget;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    QNetworkAccessManager m_networkManager;
    QFrame *mp_sortFrame = nullptr;
    QFrame *mp_urlFrame = nullptr;
    QLabel *mp_serverLabel = nullptr;
    QLineEdit *mp_urlEdit = nullptr;

    QFrame *mp_arrayFrame = nullptr;
    QPlainTextEdit *mp_numberArray = nullptr;
    QToolButton *mp_sortRequestButton = nullptr;

    QPlainTextEdit *mp_sortedNumberArray = nullptr;

    QStatusBar *mp_statusBar = nullptr;
    QProgressBar *mp_progressBar = nullptr;

    QTableWidget *mp_sortHistoryView = nullptr;
    QPlainTextEdit *mp_currentText = nullptr;
};

#endif // MAINWINDOW_H
